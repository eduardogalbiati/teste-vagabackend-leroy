<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\Http\Requests;
use App\Jobs\ImportProducts;
use Storage;

class ProductsController extends Controller
{

    /**
     * GET /.
     */
    public function index()
    {
    	return view('products.index');
    }

    /**
     * GET /api/products.
     */
    public function get()
    {
        try{
    	   $products = Product::all();
        }catch(Exception $e){
            return \Response::make($e->getMessage(), 400);
        }

        return $products;
    }

    /**
     * GET /api/products/{id}.
     */
    public function show(Product $product)
    {
    	return $product;
    }

    /**
     * POST /api/products.
     */
    public function store(Request $request)
    {
        try{
            $file = $request->file('file');
            $fileName = date("Y_m_d").'_'.time();
            
            //storing file in storage
            $file->storeAs('filesUpload/',$fileName);

            //dispatching the job
            dispatch(new ImportProducts('filesUpload/'.$fileName));
        }catch(Exception $e){
            return \Response::make($e->getMessage(), 400);
        }

        return \Response::make('Processing your file', 200);
    }

    /**
     * PATCH /api/products/{id}.
     */
    public function update(Request $request, $id)
    {
        try{
            $product = Product::findOrFail($id);
        }catch(Exception $e){
            return \Response::make($e->getMessage(), 404);
        }

    	try{
	    	$product->update([
	    		'name' => $request->get('name'),
	    		'description' => $request->get('description'),
	    		'free_shiping' => $request->get('free_shiping', 0),
	    		'category' => $request->get('category'),
	    		'value' => $request->get('value'),
	    		]);
    	}catch(Exception $e){
    		return \Response::make($e->getMessage(), 400);
    	}

    	return $product;
    }

    /**
     * DELETE /api/products/{id}.
     */
    public function destroy($id)
    {

        try{
            $product = Product::findOrFail($id);
        }catch(Exception $e){
            return \Response::make($e->getMessage(), 404);
        }

    	try{
    		$product->delete();
    	}catch(Exception $e){
    		return \Response::make($e->getMessage(), 400);
    	}

    	return \Response::make('Entry succesfully removed', 200);
    }
}
