<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;
use Storage;
use PHPExcel_IOFactory;
use App\Product;

class ImportProducts implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $file;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Getting file path
        $filePath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        $filePath .= $this->file;

        //loading into PHPEXcel
        $excelObj = PHPExcel_IOFactory::load($filePath);
        $ws = $excelObj->getActiveSheet();

        $category = $ws->getCell('B2')->getValue();

        for($i = 5; $ws->getCell('A'.$i) != ''; $i++){
            $id = $ws->getCell('A'.$i);

            $product = Product::firstOrNew(['id'=>$id]);

            $product->category = $category;
            $product->id = $id;
            $product->name = $ws->getCell('B'.$i);
            $product->free_shiping = $ws->getCell('C'.$i);
            $product->description = $ws->getCell('D'.$i);
            $product->value = $ws->getCell('E'.$i);

            $product->save();
        }

    }
}
