# CRUD (Queue, Tests and Build)

## EXT Dependencies
1. `sudo apt-get install php7.0-sqlite`
2. `sudo apt-get install php7.0-zip`

## Features
1. CRUD (Eloquent)
2. Queue
3. Unit Testing (Mockery)
4. Build (Console)

## Getting started
1. Run `composer install`
2. Build `php artisan build:run`. This command will create/migrate Database, configure .env and run the queue server
3. Into another terminal run `php artisan serve`

## Testing
	`./vendor/bin/phpunit`
    
## Known Issues
1. `mockery` is not mocking my eloquent model, i cant figured out why, searching on goole i found a lot of examples for laravel4. But all the structure and concept of Controller test was written on test class

2. `queue` for now i dont have a way to show the user that the file has been processed, i could create a `Websocket` connection,  or use `Firebase` to real time communication
3. Logic on controller, i should remove the logic rules and put into a service. The Controller should just delegate the action to service, and be responsible  only for the HTTP responses

