@extends('layout');

@section('content')
<div class="page-header">
        <h1>Products List</h1>
        <button class="btn btn-primary" onclick="$('#file').click()"><span class="glyphicon glyphicon-cloud"></span>Upload XLXS</button>
        <button class="btn btn-default" onclick="loadEntries();"><span class="glyphicon glyphicon-refresh"></span>Refresh</button>
        <div style="display:none;">
        	<input type="file" id="file">
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <table class="table">
	        <thead>
	          <tr>
	              <th>id</th>
	              <th>Name</th>
	              <th>Description</th>
	              <th>Free Shiping</th>
	              <th>Value</th>
	              <th>Options</th>
	          </tr>
	        </thead>

	        <tbody>
	        	
	        </tbody>
          </table>
        </div>
      
      </div>

      <!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="myModalLabel">Edit</h4>
		      </div>
		      <div class="modal-body">
		        <div id="alert_entry" class="alert alert-info">Loading...</div>
		          <div id="entryForm" style="display:none;">
		          	<input type="hidden" name="id_product" id="id_product">
		        	<form id="form_entry" class="form">
		        		  	{{method_field('PATCH')}}

		        	  <div class="form-group">
						<label for="name">Name</label>
						<input placeholder="Placeholder" id="name" name="name" type="text" class="form-control">
					  </div>

					  <div class="form-group">
				    	<label for="value">Value</label>
					    <input id="value" name="value" type="text" class="form-control" >
					  </div>

					  <div class="form-group">
				    	<label for="description">Description</label>
						<input id="description" name="description" type="text" class="form-control" >
					  </div>

					  <div class="form-group">
				    	<label for="category">Category</label>
						<input id="category" name="category" type="text" class="form-control" >
					  </div>
						  
					  <div class="checkbox">
					    <label>
					      <input type="checkbox" id="free_shiping" name="free_shiping" value="1"/>
				     		Free Shiping
					    </label>
					  </div>
					</form>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" onclick="updateEntry();" class="btn btn-primary">Save changes</button>
		      </div>
		    </div>
		  </div>
		</div>
@stop

@section('scripts')
<script>
	$(document).ready(function(){
		loadEntries();
	})

	// Load entries function
	function loadEntries(){
		$('tbody').html('<div class="alert alert-info">Loading...</div>');
		$.ajax(
	    		{
		        url: '/api/products',
		        type: 'GET',
		        headers: {
				    'X-CSRF-TOKEN': '{{ csrf_token() }}'
				  },
		        success: function (data) { loadEntriesSuccess(data) },
		        error: function (error) { console.log(error) }
	    	});
	}
	function loadEntriesSuccess(data){
		var tpl = '';
		$('tbody').html('');
		for (var x in data){
			tpl  = '<tr>'+
					 '<td>'+data[x].id+'</td>' +
					 '<td>'+data[x].name+'</td>' +
					 '<td>'+data[x].description+'</td>' +
					 '<td>'+data[x].free_shiping+'</td>' +
					 '<td>'+data[x].value+'</td>' +
			         '<td class="table-options"><button type="button" class="btn btn-sm btn-default" data-toggle="modal"' +
			         'data-target="#myModal" onClick="showEntry('+data[x].id+')">Edit</button>' +
			         '<button type="button" class="btn btn-sm btn-default" onClick="deleteEntry('+data[x].id+')">' +
			         'Delete</button></td>';
		          	'</tr>';
		    $('tbody').append(tpl);
		}

	}

	function loadEntriesError(error){
		alert('Error loading products')
	}



	// delete entry function
	function deleteEntry(id){
		$.ajax(
    		{
	        url: '/api/products/'+id,
	        type: 'DELETE',
	        headers: {
			    'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
	        success: function (data) { deleteEntrySuccess() },
	        error: function (error) { alert(error.responseText) }
    	});

	}

	function deleteEntrySuccess(){
		alert('Entry deleted Successfuly');
		loadEntries();
	}


	// show entry function
	function showEntry(id){
		$.ajax(
    		{
	        url: '/api/products/'+id,
	        type: 'GET',
	        headers: {
			    'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
	        success: function (data) {

	        	showEntrySuccess(data)
	        },
	        error: function (error) { 
	        	showEntryError(error) 
	        }
    	});

	}

	function showEntrySuccess(data){

		$('#alert_entry').hide();
		$('#entryForm').show();
		$('#name').val(data.name);
		$('#value').val(data.value);
		$('#description').val(data.description);
		$('#category').val(data.category);
		$('#id_product').val(data.id);
		if(data.free_shiping == 1){
			$('#free_shiping').attr('checked', true);
		}else{
			$('#free_shiping').attr('checked', false);
		}
			
	}

	function showEntryError(error){
		$('#alert_entry').removeClass().addClass('alert alert-danger').show();
		$('#alert_entry').html('<b>Error</b>' + error.responseText )
	}


	// edit entry function
	function updateEntry(){
		$.ajax(
    		{
	        url: '/api/products/'+$('#id_product').val(),
	        type: 'POST',
	        data: $('#form_entry').serialize(),
	        headers: {
			    'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
	        success: function (data) {
	        	alert('Successfuly saved');
	        	$('#myModal').modal('hide');
	        	loadEntries();
	        },
	        error: function (error) {
	        	alert('Error updating Entry' + error.responseText)
	        }
    	});

	}

	$('#file').change(function(){
		if($(this).val() != '')
		uploadFile();
	})

	function uploadFile(){
		var formData = new FormData();
		formData.append('file', $('#file')[0].files[0]);

		$.ajax({
		       url : '/api/products',
		       type : 'POST',
		       data : formData,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(data) {
		       	$('#file').val('');
		           console.log(data);
		           alert('Your file is beeing processed, press the refresh button to check the results');
		       },
		       error: function (error) {
		       	$('#file').val('')
		         console.log(error);
	        	alert('Error uploading file' + error.responseText)
	           }
		});
	}
</script>
@stop