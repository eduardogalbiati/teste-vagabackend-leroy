<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('products/create', 'ProductsController@create');

Route::get('products', 'ProductsController@get');

Route::post('products', 'ProductsController@store');

Route::get('products/{product}', 'ProductsController@show');

Route::patch('products/{product}', 'ProductsController@update');

Route::delete('products/{product}', 'ProductsController@destroy');
