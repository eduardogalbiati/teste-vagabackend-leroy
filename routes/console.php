<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('build:install', function () {
	$this->info('Installing...');
	$this->comment('  > Copying files...');
	File::copy('.env.example', '.env');
	File::put('database/database.sqlite', '');
	sleep(1);
	$this->comment('  > Running migrations...');
    Artisan::call('migrate');


	$this->comment('  > Clearing cache...');
    Artisan::call('config:clear');
    Artisan::call('cache:clear');
	$this->info('Done...');
})->describe('Installing ');


Artisan::command('build:run', function () {
	
	$this->info('Installing...');
	Artisan::call('build:install');
	$this->info('Done...');

    $this->info('Running queue server...');
    $this->info("leave this window opened, and run the server with 'php artisan serve' on another terminal");

    Artisan::call('queue:listen');

})->describe('Running Queue ');
