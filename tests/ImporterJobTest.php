<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Jobs\ImportProducts;

class ImporterJobTest extends TestCase
{

	use DatabaseMigrations;
	use DatabaseTransactions;

    public function testJobShouldGetAFileNameAndInsertOnDatabase()
    {

    	$imp = new ImportProducts('public/examples/example.xlsx');
    	$imp->handle();
    	
    	$this->seeInDatabase('products', [
        	'id' => '1001',
        	'name' => 'Furadeira X',
        	'free_shiping' => 0,
        	'description' => 'Furadeira eficiente X',
        	'value' => '100.00',
        	'category' => 'Ferramentas',
    	]);

    	$this->seeInDatabase('products', [
        	'id' => '1002',
        	'name' => 'Furadeira Y',
        	'free_shiping' => 1,
        	'description' => 'Furadeira super eficiente Y',
        	'value' => '140.00',
        	'category' => 'Ferramentas',
    	]);

    	$this->seeInDatabase('products', [
        	'id' => '1010',
        	'name' => 'Luvas de Proteção',
        	'free_shiping' => 0,
        	'description' => 'Luva de proteção básica',
        	'value' => '5.60',
        	'category' => 'Ferramentas',
    	]);

        $this->assertTrue(true);
    }


}
