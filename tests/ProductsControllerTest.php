<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductsControllerTest extends TestCase
{
	protected $productMock;
    
    public function setUp()
    {
    	parent::setUp();
        $this->productMock = Mockery::mock('Eloquent', 'App\\Product');
    }

    public function testIndex()
	{
	    $response = $this->call('GET', '/');
        $this->assertEquals(200, $response->getStatusCode());
	}

	public function testGet()
	{
	    $response = $this->call('GET', 'api/products');
        $this->assertEquals(200, $response->getStatusCode());
	}

	public function testPatch()
	{
		$this->productMock->shouldReceive('findOrFail')
        	->once()
        	->andReturn([]);

        $this->productMock->shouldReceive('update')
            ->once()
            ->andReturn([]);

        $this->app->instance('App\\Product', $this->productMock);
       
	    $response = $this->call('PATCH', 'api/products/1');

        $this->assertEquals(200, $response->getStatusCode());
	}

	public function testDestroy()
	{
		$mock = Mockery::mock('Eloquent', 'App\\Product');

        $mock->shouldReceive('delete')
            ->once()
            ->andReturn([]);

        $mock->shouldReceive('findOrFail')
        	->once()
        	->andReturn([]);

        $this->app->instance('App\\Product', $mock);
       
	    $response = $this->call('DELETE', 'api/products/1');

        $this->assertEquals(200, $response->getStatusCode());
	}

	public function tearDown()
	{
	    Mockery::close();
	}
}
